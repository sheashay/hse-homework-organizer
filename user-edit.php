<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
  <?php $title = 'Users' ?>
  <?php require("partials/head.php"); ?>
  </head>
  <body>
	<?php require("partials/site-navbar.php"); ?>
	<?php require("partials/site-menubar.php"); ?>
	<div class="page animsition">
	<div class="page-header">
			<h1 class="page-title">User Edit</h1>
			<div class="page-header-actions"> </div>
		</div>
	<div class="page-content">
			<div class="panel">
			<div class="panel-body container-fluid">
					<div class="example-wrap margin-lg-0">
					<div class="nav-tabs-horizontal">
							<ul class="nav nav-tabs nav-tabs-line" data-plugin="nav-tabs" role="tablist">
							<li class="active" role="presentation"><a data-toggle="tab" href="#accountSettings" aria-controls="accountSettings"
                      role="tab">Settings</a></li>
							<li role="presentation"><a data-toggle="tab" href="#accountClasses" aria-controls="accountClasses"
                      role="tab">Classes</a></li>
						</ul>
							<div class="tab-content padding-top-20">
							<div class="tab-pane active" id="accountSettings" role="tabpanel">
									<div class="example-wrap">
									<h4 class="example-title">Edit John Doe</h4>
									<div class="example">
											<form autocomplete="off">
											<div class="form-group form-material row">
													<div class="col-sm-6">
													<label class="control-label" for="inputBasicFirstName">First Name</label>
													<input type="text" class="form-control" id="inputBasicFirstName" name="inputFirstName"
                        placeholder="First Name" autocomplete="off" />
												</div>
													<div class="col-sm-6">
													<label class="control-label" for="inputBasicLastName">Last Name</label>
													<input type="text" class="form-control" id="inputBasicLastName" name="inputLastName"
                        placeholder="Last Name" autocomplete="off" />
												</div>
												</div>
											<div class="form-group form-material">
													<label class="control-label" for="inputBasicEmail">Email Address</label>
													<input type="email" class="form-control" id="inputBasicEmail" name="inputEmail"
                      placeholder="Email Address" autocomplete="off" />
												</div>
											<div class="form-group form-material">
													<label class="control-label">Status</label>
													<div>
													<div class="radio-custom radio-default radio-inline">
															<input type="radio" id="statusActive" name="status" checked />
															<label for="statusActive">Active</label>
														</div>
													<div class="radio-custom radio-default radio-inline">
															<input type="radio" id="statusInactive" name="status"  />
															<label for="statusInactive">Inactive</label>
														</div>
												</div>
												</div>
											<div class="form-group form-material">
													<button type="button" class="btn btn-primary">Save</button>
												</div>
										</form>
										</div>
								</div>
								</div>
							<div class="tab-pane" id="accountClasses" role="tabpanel"> Sole, latinas incurreret optari vivatur, porro delectu epicurus cadere impedit
									fit ferreum concludaturque varias, omnium gloriosis vivendo
									via filio contentam expeteretur fonte expectata, quosque
									praetor quid iucunditatis fortitudinem esse. </div>
						</div>
						</div>
				</div>
				</div>
		</div>
		</div>
</div>
	<?php require("partials/site-footer.php"); ?>
	<?php require("partials/javascripts.php"); ?>
</body>
</html>