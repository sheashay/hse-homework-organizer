/*!
 * Remark (http://getbootstrapadmin.com/Remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
$.components.register("asColorPicker", {
  defaults: {
    namespace: "colorInputUi"
  },
  mode: "default"
});
