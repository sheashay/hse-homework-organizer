/*!
 * Remark (http://getbootstrapadmin.com/Remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
$.components.register("selectpicker", {
  mode: "default",
  defaults: {
    style: "btn-select",
    iconBase: "icon",
    tickIcon: "md-check"
  }
});
