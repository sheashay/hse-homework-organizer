/*!
 * Remark (http://getbootstrapadmin.com/Remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
$.components.register("datepair", {
  mode: "default",
  defaults: {
    startClass: 'datepair-start',
    endClass: 'datepair-end',
    timeClass: 'datepair-time',
    dateClass: 'datepair-date'
  }
});
