/*!
 * Remark (http://getbootstrapadmin.com/Remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
$.components.register("magnificPopup", {
  mode: "default",
  defaults: {
    type: "image",
    closeOnContentClick: true,
    image: {
      verticalFit: true
    }
  }
});
