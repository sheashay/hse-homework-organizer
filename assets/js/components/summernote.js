/*!
 * Remark (http://getbootstrapadmin.com/Remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
$.components.register("summernote", {
  mode: "default",
  defaults: {
    height: 300
  }
});
