/*!
 * Remark (http://getbootstrapadmin.com/Remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
$.components.register("tagsinput", {
  defaults: {
    tagClass: "label label-default"
  },
  mode: "default"
});
