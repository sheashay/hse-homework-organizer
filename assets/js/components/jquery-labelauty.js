/*!
 * Remark (http://getbootstrapadmin.com/Remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
$.components.register("labelauty", {
  mode: "default",
  defaults: {
    same_width: true
  }
});
