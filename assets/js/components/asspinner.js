/*!
 * Remark (http://getbootstrapadmin.com/Remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
$.components.register("asSpinner", {
  mode: "default",
  defaults: {
    namespace: "spinnerUi",
    skin: null,
    min: "-10",
    max: 100,
    mousewheel: true
  }
});
