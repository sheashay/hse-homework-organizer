<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
  <?php $title = 'Datatable Grid' ?>
  <?php require("partials/head.php"); ?>
  </head>
  <body>
	<?php require("partials/site-navbar.php"); ?>
	<?php require("partials/site-menubar.php"); ?>
	<div class="page animsition">
	<div class="page-header">
			<h1 class="page-title">Datatable Grid</h1>
			<div class="page-header-actions"> </div>
		</div>
	<div class="page-content">
			<div class="panel">
			<header class="panel-heading">
					<div class="panel-actions"></div>
					<h3 class="panel-title">Grid List</h3>
				</header>
			<div class="panel-body">
					<table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
					<thead>
							<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Signup Date</th>
							<th>Actions</th>
						</tr>
						</thead>
					<tfoot>
						</tfoot>
					<tbody>
							<tr>
							<td>Damon</td>
							<td>name@email.com</td>
							<td>2014/06/13</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Torrey</td>
							<td>name@email.com</td>
							<td>2014/09/12</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Miracle</td>
							<td>name@email.com</td>
							<td>2013/09/27</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Wilhelmine</td>
							<td>name@email.com</td>
							<td>2013/06/28</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Hubert</td>
							<td>name@email.com</td>
							<td>2013/05/28</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Myrtie.Gerhold</td>
							<td>name@email.com</td>
							<td>2014/12/12</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Chester</td>
							<td>name@email.com</td>
							<td>2014/09/27</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Melany_Gerhold</td>
							<td>name@email.com</td>
							<td>2014/06/28</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Thea</td>
							<td>name@email.com</td>
							<td>2014/11/12</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Albin.Kreiger</td>
							<td>name@email.com</td>
							<td>2013/11/27</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Shanel</td>
							<td>name@email.com</td>
							<td>2015/04/28</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Bell.Mueller</td>
							<td>name@email.com</td>
							<td>2013/10/12</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Clementina</td>
							<td>name@email.com</td>
							<td>2013/11/12</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Johanna.Thiel</td>
							<td>name@email.com</td>
							<td>2013/12/27</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Elliott_Becker</td>
							<td>name@email.com</td>
							<td>2014/08/28</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
							<tr>
							<td>Yasmine</td>
							<td>name@email.com</td>
							<td>2014/12/12</td>
							<td class="actions"><a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                  data-toggle="tooltip" data-original-title="Save"><i class="icon md-wrench" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                  data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                  data-toggle="tooltip" data-original-title="Remove"><i class="icon md-delete" aria-hidden="true"></i></a></td>
						</tr>
						</tbody>
				</table>
				</div>
		</div>
		</div>
</div>
	<?php require("partials/site-footer.php"); ?>
	<?php require("partials/javascripts.php"); ?>
</body>
</html>