<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
  <?php $title = 'Home' ?>
  <?php require("partials/head.php"); ?>
  </head>
  <body>
	<?php require("partials/site-navbar.php"); ?>
	<?php require("partials/site-menubar.php"); ?>
	<div class="page animsition">
	<div class="page-header">
			<h1 class="page-title">Home</h1>
			<div class="page-header-actions"> </div>
		</div>
	<div class="page-content">
			<p>Page content goes here</p>
		</div>
</div>
	<?php require("partials/site-footer.php"); ?>
	<?php require("partials/javascripts.php"); ?>
</body>
</html>