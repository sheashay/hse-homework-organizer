<!-- Core  -->
<script src="assets/vendor/jquery/jquery.js"></script>
<script src="assets/vendor/bootstrap/bootstrap.js"></script>
<script src="assets/vendor/animsition/animsition.js"></script>
<script src="assets/vendor/asscroll/jquery-asScroll.js"></script>
<script src="assets/vendor/mousewheel/jquery.mousewheel.js"></script>
<script src="assets/vendor/asscrollable/jquery.asScrollable.all.js"></script>
<script src="assets/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
<script src="assets/vendor/waves/waves.js"></script>
<!-- Plugins -->
<script src="assets/vendor/switchery/switchery.min.js"></script>
<script src="assets/vendor/intro-js/intro.js"></script>
<script src="assets/vendor/screenfull/screenfull.js"></script>
<script src="assets/vendor/slidepanel/jquery-slidePanel.js"></script>
<script src="assets/vendor/aspaginator/jquery.asPaginator.min.js"></script>
<!-- Datatables -->
<script src="assets/vendor/datatables/jquery.dataTables.js"></script>
<script src="assets/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="assets/vendor/asrange/jquery-asRange.min.js"></script>
<script src="assets/vendor/bootbox/bootbox.js"></script>

<!-- Scripts -->
<script src="assets/js/core.js"></script>
<script src="assets/js/site.js"></script>
<script src="assets/js/sections/menu.js"></script>
<script src="assets/js/sections/menubar.js"></script>
<script src="assets/js/sections/sidebar.js"></script>
<script src="assets/js/components/asscrollable.js"></script>
<script src="assets/js/components/animsition.js"></script>
<script src="assets/js/components/slidepanel.js"></script>
<script src="assets/js/components/switchery.js"></script>
<script src="assets/js/components/tabs.js"></script>
<script src="assets/js/components/aspaginator.js"></script>
<script src="assets/js/plugins/responsive-tabs.js"></script>
<script src="assets/js/components/datatables.js"></script>
<script src="assets/examples/js/tables/datatable.js"></script>
<script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>