<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
  <?php $title = 'Users' ?>
  <?php require("partials/head.php"); ?>
  </head>
  <body class="page-user">
	<?php require("partials/site-navbar.php"); ?>
	<?php require("partials/site-menubar.php"); ?>
	<div class="page animsition">
	<div class="page-header">
			<h1 class="page-title">Users</h1>
			<div class="page-header-actions"> </div>
		</div>
	<div class="page-content">
			<div class="panel">
			<div class="panel-body">
					<form class="page-search-form" role="search">
					<div class="input-search input-search-dark"> <i class="input-search-icon md-search" aria-hidden="true"></i>
							<input type="text" class="form-control" id="inputSearch" name="search" placeholder="Search Users">
							<button type="button" class="input-search-close icon md-close" aria-label="Close"></button>
						</div>
				</form>
					<div class="nav-tabs-horizontal">
					<div class="dropdown page-user-sortlist"> Order By: <a class="dropdown-toggle inline-block" data-toggle="dropdown"
              href="#" aria-expanded="false">Last Active<span class="caret"></span></a>
							<ul class="dropdown-menu animation-scale-up animation-top-right animation-duration-250"
              role="menu">
							<li class="active" role="presentation"><a href="javascript:void(0)">Last Active</a></li>
							<li role="presentation"><a href="javascript:void(0)">Username</a></li>
							<li role="presentation"><a href="javascript:void(0)">Register Date</a></li>
						</ul>
						</div>
					<ul class="nav nav-tabs nav-tabs-line" data-plugin="nav-tabs" role="tablist">
							<li class="active" role="presentation"><a data-toggle="tab" href="#all-users" aria-controls="all-users"
                role="tab">All</a></li>
							<li role="presentation"><a data-toggle="tab" href="#inactive-users" aria-controls="inactive-users"
                role="tab">Inactive</a></li>
						</ul>
					<div class="tab-content">
							<div class="tab-pane animation-fade active" id="all-users" role="tabpanel">
							<ul class="list-group">
									<li class="list-group-item">
									<div class="media">
											<div class="media-left">
											<div class="avatar"> <img src="http://placehold.it/150x150?text=User" alt="..."> </div>
										</div>
											<div class="media-body">
											<h4 class="media-heading"> Caleb Richards <small>Last Access: 11 hours ago</small> </h4>
											<p> <i class="icon icon-color md-email" aria-hidden="true"></i> name@email.com <i class="icon icon-color md-calendar margin-left-15" aria-hidden="true"></i> 04/01/2016</p>
										</div>
											<div class="media-right"> <a class="btn btn-pure btn-info icon md-edit waves-effect waves-circle waves-classic" href="user-edit.php"></a> <a class="btn btn-pure btn-danger icon md-block waves-effect waves-circle waves-classic" href="#nogo"></a> </div>
										</div>
								</li>
									<li class="list-group-item">
									<div class="media">
											<div class="media-left">
											<div class="avatar"> <img src="http://placehold.it/150x150?text=User" alt="..."> </div>
										</div>
											<div class="media-body">
											<h4 class="media-heading"> Mary Adams <small>Last Access: 11 hours ago</small> </h4>
											<p> <i class="icon icon-color md-email" aria-hidden="true"></i> name@email.com <i class="icon icon-color md-calendar margin-left-15" aria-hidden="true"></i> 04/01/2016</p>
										</div>
											<div class="media-right"> <a class="btn btn-pure btn-info icon md-edit waves-effect waves-circle waves-classic" href="user-edit.php"></a> <a class="btn btn-pure btn-danger icon md-block waves-effect waves-circle waves-classic" href="#nogo"></a> </div>
										</div>
								</li>
									<li class="list-group-item">
									<div class="media">
											<div class="media-left">
											<div class="avatar"> <img src="http://placehold.it/150x150?text=User" alt="..."> </div>
										</div>
											<div class="media-body">
											<h4 class="media-heading"> Caleb Richards <small>Last Access: 11 hours ago</small> </h4>
											<p> <i class="icon icon-color md-email" aria-hidden="true"></i> name@email.com <i class="icon icon-color md-calendar margin-left-15" aria-hidden="true"></i> 04/01/2016</p>
										</div>
											<div class="media-right"> <a class="btn btn-pure btn-info icon md-edit waves-effect waves-circle waves-classic" href="user-edit.php"></a> <a class="btn btn-pure btn-danger icon md-block waves-effect waves-circle waves-classic" href="#nogo"></a> </div>
										</div>
								</li>
									<li class="list-group-item">
									<div class="media">
											<div class="media-left">
											<div class="avatar"> <img src="http://placehold.it/150x150?text=User" alt="..."> </div>
										</div>
											<div class="media-body">
											<h4 class="media-heading"> Mary Adams <small>Last Access: 11 hours ago</small> </h4>
											<p> <i class="icon icon-color md-email" aria-hidden="true"></i> name@email.com <i class="icon icon-color md-calendar margin-left-15" aria-hidden="true"></i> 04/01/2016</p>
										</div>
											<div class="media-right"> <a class="btn btn-pure btn-info icon md-edit waves-effect waves-circle waves-classic" href="user-edit.php"></a> <a class="btn btn-pure btn-danger icon md-block waves-effect waves-circle waves-classic" href="#nogo"></a> </div>
										</div>
								</li>
									<li class="list-group-item">
									<div class="media">
											<div class="media-left">
											<div class="avatar"> <img src="http://placehold.it/150x150?text=User" alt="..."> </div>
										</div>
											<div class="media-body">
											<h4 class="media-heading"> Caleb Richards <small>Last Access: 11 hours ago</small> </h4>
											<p> <i class="icon icon-color md-email" aria-hidden="true"></i> name@email.com <i class="icon icon-color md-calendar margin-left-15" aria-hidden="true"></i> 04/01/2016</p>
										</div>
											<div class="media-right"> <a class="btn btn-pure btn-info icon md-edit waves-effect waves-circle waves-classic" href="user-edit.php"></a> <a class="btn btn-pure btn-danger icon md-block waves-effect waves-circle waves-classic" href="#nogo"></a> </div>
										</div>
								</li>
									<li class="list-group-item">
									<div class="media">
											<div class="media-left">
											<div class="avatar"> <img src="http://placehold.it/150x150?text=User" alt="..."> </div>
										</div>
											<div class="media-body">
											<h4 class="media-heading"> Mary Adams <small>Last Access: 11 hours ago</small> </h4>
											<p> <i class="icon icon-color md-email" aria-hidden="true"></i> name@email.com <i class="icon icon-color md-calendar margin-left-15" aria-hidden="true"></i> 04/01/2016</p>
										</div>
											<div class="media-right"> <a class="btn btn-pure btn-info icon md-edit waves-effect waves-circle waves-classic" href="user-edit.php"></a> <a class="btn btn-pure btn-danger icon md-block waves-effect waves-circle waves-classic" href="#nogo"></a> </div>
										</div>
								</li>
								</ul>
							<nav>
									<ul data-plugin="paginator" data-total="50" data-skin="pagination-no-border">
								</ul>
								</nav>
						</div>
							<div class="tab-pane animation-fade" id="inactive-users" role="tabpanel">
							<ul class="list-group">
									<li class="list-group-item">
									<div class="media">
											<div class="media-left">
											<div class="avatar"> <img src="http://placehold.it/150x150?text=User" alt="..."> </div>
										</div>
											<div class="media-body">
											<h4 class="media-heading"> Caleb Richards <small>Last Access: 11 hours ago</small> </h4>
											<p> <i class="icon icon-color md-email" aria-hidden="true"></i> name@email.com <i class="icon icon-color md-calendar margin-left-15" aria-hidden="true"></i> 04/01/2016</p>
										</div>
											<div class="media-right"> <a class="btn btn-pure btn-info icon md-edit waves-effect waves-circle waves-classic" href="user-edit.php"></a> <a class="btn btn-pure btn-danger icon md-block waves-effect waves-circle waves-classic" href="#nogo"></a> </div>
										</div>
								</li>
									<li class="list-group-item">
									<div class="media">
											<div class="media-left">
											<div class="avatar"> <img src="http://placehold.it/150x150?text=User" alt="..."> </div>
										</div>
											<div class="media-body">
											<h4 class="media-heading"> Mary Adams <small>Last Access: 11 hours ago</small> </h4>
											<p> <i class="icon icon-color md-email" aria-hidden="true"></i> name@email.com <i class="icon icon-color md-calendar margin-left-15" aria-hidden="true"></i> 04/01/2016</p>
										</div>
											<div class="media-right"> <a class="btn btn-pure btn-info icon md-edit waves-effect waves-circle waves-classic" href="user-edit.php"></a> <a class="btn btn-pure btn-danger icon md-block waves-effect waves-circle waves-classic" href="#nogo"></a> </div>
										</div>
								</li>
								</ul>
							<nav>
									<ul data-plugin="paginator" data-total="50" data-skin="pagination-no-border">
								</ul>
								</nav>
						</div>
						</div>
				</div>
				</div>
		</div>
		</div>
</div>
	<?php require("partials/site-footer.php"); ?>
	<?php require("partials/javascripts.php"); ?>
</body>
</html>